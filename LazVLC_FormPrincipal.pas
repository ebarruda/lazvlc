unit LazVLC_FormPrincipal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ComCtrls, Spin, LCLProc, ExtCtrls, Menus, MaskEdit, Process;

type

  { TFormPrincipal }

  TFormPrincipal = class(TForm)
    Button1: TButton;
    Button2: TButton;
    ButtonVLC: TButton;
    ButtonParar: TButton;
    ButtonExecutar: TButton;
    ButtonFechar: TButton;
    CheckBoxRepetirExecucao: TCheckBox;
    CheckBoxTelaCheia: TCheckBox;
    CheckBoxOcultarInterface: TCheckBox;
    CheckBoxExecutarVlcIniciar: TCheckBox;
    ComboBoxTV: TComboBox;
    ComboBoxVLCParametros: TComboBox;
    EditVLC: TEdit;
    Label1: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    LabelBotaoAcao2: TLabel;
    LabelBotaoAcao3: TLabel;
    ListBox1: TListBox;
    MaskEditHorarioFim: TMaskEdit;
    MaskEditHorarioInicio: TMaskEdit;
    Memo1: TMemo;
    MenuItemExecutar: TMenuItem;
    MenuItemParar: TMenuItem;
    MenuItemSep2: TMenuItem;
    MenuItemSair: TMenuItem;
    MenuItemSep1: TMenuItem;
    MenuItemExibir: TMenuItem;
    MenuItemOcultar: TMenuItem;
    OpenDialogVideo: TOpenDialog;
    OpenDialogVLC: TOpenDialog;
    PageControl1: TPageControl;
    PopupMenuLazVLC: TPopupMenu;
    SpinEditEsquerda: TSpinEdit;
    SpinEditTopo: TSpinEdit;
    SpinEditLargura: TSpinEdit;
    SpinEditAltura: TSpinEdit;
    SpinEditVLCVolume: TSpinEdit;
    TabSheet1: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet2: TTabSheet;
    TrayIconVLC: TTrayIcon;
    procedure ButtonFecharClick(Sender: TObject);
    procedure ButtonExecutarClick(Sender: TObject);
    procedure ButtonPararClick(Sender: TObject);
    procedure ButtonVLCClick(Sender: TObject);
    procedure ComboBoxTVDblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MenuItemExecutarClick(Sender: TObject);
    procedure MenuItemExibirClick(Sender: TObject);
    procedure MenuItemOcultarClick(Sender: TObject);
    procedure MenuItemPararClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
    AoIniciar: Boolean;
    ArquivoXml: String;
    VLCEXE: String;
    Esquerda: SmallInt;
    Topo: SmallInt;
    Largura: SmallInt;
    Altura: SmallInt;
    ProcessVLC: TProcess;
    procedure ExecutarVLC;
  end;

var
  FormPrincipal: TFormPrincipal;

implementation

{$R *.lfm}

{ TFormPrincipal }

procedure TFormPrincipal.FormCreate(Sender: TObject);
var
  ConteudoXml: String;
  StringListXml: TStringList;
begin
  AoIniciar := true;

  Left := (Screen.Width  div 2) - (Width div 2);
  Top  := (Screen.Height div 2) - (Height div 2);

  ProcessVLC := TProcess.Create(nil);

  PageControl1.TabIndex := 0;

  Esquerda := 100;
  Topo     := 100;
  Largura  := 320;
  Altura   := 240;
  VLCEXE   := 'vlc.exe';

  ComboBoxTV.Clear;

  ArquivoXml := GetEnvironmentVariableUTF8('appdata') + DirectorySeparator + 'LazVLC.xml';

  StringListXml := TStringList.Create;

  if not FileExistsUTF8(ArquivoXml) then begin
    StringListXml.Add('<?xml version="1.0" encoding="UTF-8" ?>');
    StringListXml.Add('<vlcexe>' + VLCEXE + '</vlcexe>');
    StringListXml.Add('<esquerda>' + IntToStr(Esquerda) + '</esquerda>');
    StringListXml.Add('<topo>' + IntToStr(Topo) + '</topo>');
    StringListXml.Add('<largura>' + IntToStr(Largura) + '</largura>');
    StringListXml.Add('<altura>' + IntToStr(Altura) + '</altura>');
    StringListXml.Add('<canais>');

    ComboBoxTV.Items.Add('TV Universitária <mms://galileo.netpoint.com.br/cnuvitoria>');
    ComboBoxTV.Items.Add('Nasa <http://www.nasa.gov/145590main_Digital_Media.asx>');
    ComboBoxTV.Items.Add('Nasa Educacional<http://www.nasa.gov/145588main_Digital_Edu.asx>');
    ComboBoxTV.Items.Add('TV Senado <mms://drix.senado.gov.br/tv1>');
    ComboBoxTV.Items.Add('TV Senado <mms://drix.senado.gov.br/tv2>');
    ComboBoxTV.Items.Add('TV Justiça <mms://187.58.204.10/tvjustica>');
    ComboBoxTV.Items.Add('TV Escola <http://refletor.mec.gov.br/tvescola>');

    if ComboBoxTV.Items.Count > 0 then begin
      ComboBoxTV.ItemIndex := ComboBoxTV.Items.Count - 1;

      repeat
        StringListXml.Add('   <url>' + ComboBoxTV.Items.Strings[ComboBoxTV.ItemIndex] + '</url>');

        ComboBoxTV.ItemIndex := ComboBoxTV.ItemIndex - 1;
      until ComboBoxTV.ItemIndex = -1;
    end;

    StringListXml.Add('</canais>');

    try
      StringListXml.SaveToFile(ArquivoXml);
    except
      ShowMessage('Erro ao salvar arquivo de configuração');
    end;
  end else begin
    try
      StringListXml.LoadFromFile(ArquivoXml);
    except
      ShowMessage('Erro ao carregar arquivo de configuração');
    end;

    ConteudoXml := StringListXml.Text;

    VLCEXE := GetPart(['<vlcexe>'], ['</vlcexe>'], ConteudoXml, False, False);

    Esquerda := StrToIntDef(GetPart(['<esquerda>'], ['</esquerda>'], ConteudoXml, False, False), 100);
    Topo     := StrToIntDef(GetPart(['<topo>'], ['</topo>'], ConteudoXml, False, False), 100);
    Largura  := StrToIntDef(GetPart(['<largura>'], ['</largura>'], ConteudoXml, False, False), 320);
    Altura   := StrToIntDef(GetPart(['<altura>'], ['</altura>'], ConteudoXml, False, False), 240);

    ComboBoxTV.Clear;

    while (Pos('url', ConteudoXml) > 0) do begin
      ComboBoxTV.Items.Add(GetPart(['<url>'], ['</url>'], ConteudoXml, False, False));

      ConteudoXml := StringReplace(ConteudoXml, '<url>' + GetPart(['<url>'], ['</url>'], ConteudoXml, False, False) + '</url>', '', [rfReplaceAll, rfIgnoreCase]);
    end;

    EditVLC.Text := VLCEXE;

    SpinEditEsquerda.Value := Esquerda;
    SpinEditTopo.Value     := Topo;
    SpinEditLargura.Value  := Largura;
    SpinEditAltura.Value   := Altura;
  end;

  StringListXml.Free;
end;

procedure TFormPrincipal.FormShow(Sender: TObject);
begin
  if AoIniciar then begin
    AoIniciar := false;

    if CheckBoxExecutarVlcIniciar.Checked then ExecutarVLC;
  end;
end;

procedure TFormPrincipal.ButtonVLCClick(Sender: TObject);
begin
  if OpenDialogVLC.Execute then begin
    VLCEXE       := OpenDialogVLC.FileName;
    EditVLC.Text := VLCEXE;
  end;
end;

procedure TFormPrincipal.ComboBoxTVDblClick(Sender: TObject);
begin
  if OpenDialogVideo.Execute then ComboBoxTV.Text := OpenDialogVideo.FileName;
end;

procedure TFormPrincipal.FormClose(Sender: TObject; var CloseAction: TCloseAction);
var
  StringListXml: TStringList;
begin
  StringListXml := TStringList.Create;

  StringListXml.Add('<?xml version="1.0" encoding="UTF-8" ?>');
  StringListXml.Add('<vlcexe>' + EditVLC.Text + '</vlcexe>');
  StringListXml.Add('<esquerda>' + IntToStr(SpinEditEsquerda.Value) + '</esquerda>');
  StringListXml.Add('<topo>' + IntToStr(SpinEditTopo.Value) + '</topo>');
  StringListXml.Add('<largura>' + IntToStr(SpinEditLargura.Value) + '</largura>');
  StringListXml.Add('<altura>' + IntToStr(SpinEditAltura.Value) + '</altura>');
  StringListXml.Add('<canais>');

  if ComboBoxTV.Items.Count > 0 then begin
    ComboBoxTV.ItemIndex := ComboBoxTV.Items.Count - 1;

    repeat
      StringListXml.Add('   <url>' + ComboBoxTV.Items.Strings[ComboBoxTV.ItemIndex] + '</url>');

      ComboBoxTV.ItemIndex := ComboBoxTV.ItemIndex - 1;
    until ComboBoxTV.ItemIndex = -1;
  end;

  StringListXml.Add('</canais>');

  try
    StringListXml.SaveToFile(ArquivoXml);
  finally
  end;

  ProcessVLC.Free;
end;

procedure TFormPrincipal.ButtonFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TFormPrincipal.ExecutarVLC;
var
  VLCParametros: String;
  VLCDimensoes: String;
  TVDescricao: String;
  TVURL: String;
begin
  if not FileExistsUTF8(EditVLC.Text) then ShowMessage('Executável do VLC não encontrado');

  if ProcessVLC.Running then ProcessVLC.Terminate(0);

  TVDescricao := ComboBoxTV.Text;

  if Length(Trim(TVDescricao)) > 0 then begin
    if (Pos('<', TVDescricao) > 0) and (Pos('>', TVDescricao) > 0) then begin
      TVURL       := Trim(GetPart(['<'], ['>'], TVDescricao, False, False));
      TVDescricao := Trim(StringReplace(TVDescricao, '<' + TVURL + '>', '', [rfReplaceAll, rfIgnoreCase]));
    end else begin
      TVURL := TVDescricao;
    end;

    if CheckBoxTelaCheia.Checked then begin
      VLCDimensoes := '--video-x=0 ' +
                      '--video-y=0 ' +
                      '--width=' + IntToStr(Screen.Width) + ' ' +
                      '--height=' + IntToStr(Screen.Height) + ' ';
    end else begin
      VLCDimensoes := '--video-x=' + IntToStr(SpinEditEsquerda.Value) + ' ' +
                      '--video-y=' + IntToStr(SpinEditTopo.Value) + ' ' +
                      '--width=' + IntToStr(SpinEditLargura.Value) + ' ' +
                      '--height=' + IntToStr(SpinEditAltura.Value) + ' ';
    end;

    VLCParametros := ComboBoxVLCParametros.Text + ' ' +
                     '--volume=' + IntToStr(SpinEditVLCVolume.Value) + ' ' +
                     VLCDimensoes +
                     BoolToStr(CheckBoxRepetirExecucao.Checked, '--loop ', '--play-and-exit ') +
                     '--video-title="' + TVDescricao + '" ' +
                     '"' + TVURL + '"';

    ProcessVLC.CommandLine := EditVLC.Text + ' ' + VLCParametros;

    ProcessVLC.Execute;

    if CheckBoxOcultarInterface.Checked then FormPrincipal.Visible := false;

    if UTF8Pos(ComboBoxTV.Text, ComboBoxTV.Items.Text) = 0 then ComboBoxTV.Items.Add(ComboBoxTV.Text);
  end;
end;

procedure TFormPrincipal.ButtonExecutarClick(Sender: TObject);
begin
  ExecutarVLC;
end;

procedure TFormPrincipal.ButtonPararClick(Sender: TObject);
begin
  if ProcessVLC.Running then ProcessVLC.Terminate(0);
end;

procedure TFormPrincipal.MenuItemExibirClick(Sender: TObject);
begin
  FormPrincipal.Visible := true;
end;

procedure TFormPrincipal.MenuItemOcultarClick(Sender: TObject);
begin
  FormPrincipal.Visible := false;
end;

procedure TFormPrincipal.MenuItemExecutarClick(Sender: TObject);
begin
  ExecutarVLC;
end;

procedure TFormPrincipal.MenuItemPararClick(Sender: TObject);
begin
  if ProcessVLC.Running then ProcessVLC.Terminate(0);
end;

end.

